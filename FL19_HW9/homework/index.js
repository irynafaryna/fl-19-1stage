// #1
function calculateSum(arr) {
  let sum = 0;
  for (let i = 0; i <= arr.length; i++) {
    sum += i;
  }
  return sum;
}

console.log(calculateSum([1,2,3,4,5])); //15

// #2
function isTriangle(a, b, c) {
  return a+b>c && a+c>b && c+b>a;
}

console.log(isTriangle(5,6,7)); //true
console.log(isTriangle(2,9,3)); //false

// #3
function isIsogram(word) {
  for(let i = 0; i <= word.length; i++) {
    for(let j = i+1; j <= word.length; j++) {
      if(word[j] === word[i]) {
          return false;
      }
    }
  }
  return true;
}

console.log(isIsogram('Dermatoglyphics')); //true
console.log(isIsogram('aab')); //false

// #4
function isPalindrome(word) {
  let reverseWord = '';
  for (let i = word.length - 1; i>=0; i--){
    reverseWord += word[i];
  }
  return reverseWord === word;
}

console.log(isPalindrome('Dermatoglyphics')); //false
console.log(isPalindrome('abbabba')); //true

// #5
function showFormattedDate(dateObj) {
  let month = ['January', 'February', 'March', 'April', 
  'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  return dateObj.getDate() + ' of ' + month[dateObj.getMonth()] + ',' + dateObj.getFullYear();
}

console.log(showFormattedDate(new Date('05/12/22'))); //'12 of May, 2022'

// #6
const letterCount = (str, letter) => {
  let sum = 0;
  for(let i=0; i<=str.length; i++){
    if(str[i] === letter){
      sum += 1;
    }
  }
  return sum;
}

console.log(letterCount('abbaba', 'b')); //3

// #7
function countRepetitions(arr) {
  let repetitions ={};
  let repcount = 1;
  let count = 1;
  for (let i = 0; i<arr.length; i=i+repcount){
    for (let j = i+1; j<arr.length; j++){
      if(arr[j]===arr[i]){
        repcount ++;
        repetitions[arr[i]] = repcount; 
      } else{
        repetitions[arr[j]] = count;
      }
    }
  }
  return repetitions;
}

console.log(countRepetitions(['banana', 'apple', 'banana'])); // { banana: 2, apple: 1 }

// #8
function calculateNumber(arr) {
  return parseInt(arr.join(''),2);
}

console.log(calculateNumber([0, 1, 0, 1])); //5
console.log(calculateNumber([1, 0, 0, 1])); //9
''