function getWeekDay(date){
  let weekdays = new Array(
      'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
  );
  let day = date.getDay();
  return weekdays[day];
}

function getAmountDaysToNewYear(){
  let thousand = 1000;
  let sixty = 60;
  let twenty = 24;
  let chrs = new Date('01/01/2023');  
  let today = new Date(Date.now());  
  let time_difference = chrs.getTime() - today.getTime(); 
  let days_difference = time_difference/(thousand * sixty * sixty * twenty); 
  return Math.round(days_difference);
}

function getApproveToPass(date){
  let eighteen = 18;
  let seventeen = 17;
  let data = 1970;
  let ageDifMs = Date.now() - date.getTime();
  let ageDate = new Date(ageDifMs);
  let age = Math.abs(ageDate.getUTCFullYear() - data);
  let years = eighteen - age;
  if(age >= eighteen){
    return 'Hello adventurer, you may pass!'
  } else if(age < eighteen && age >= seventeen){
    return 'Hello adventurer, you are to yang for this quest wait for few more months!'
  } else if(age < seventeen){
    return 'Hello adventurer, you are to yang for this quest wait for ' + years + ' years more!'
  } 
}

function transformStringToHtml(string) {
  return string.replace(/^(tag=")/g, '<').replace(/({|})/g, '"').replace(/(.value=")/g, '>').replace(/.$/g, '</>');
}

const elementP = 'tag="p" class="text" style={color: #aeaeae;} value="Aloha!"'
console.log(transformStringToHtml(elementP));

function isValidIdentifier(string){
  return /^(^\D)+[\w$]+$/g.test(string);
}

function capitalize(str) {
  return str.replace(/\w\S*/g, function(txt){
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

function isValidPassword(string){
  let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
  return regex.test(string);
}

function bubbleSort(arr){
  for(let i = 0; i < arr.length; i++){
    for (let j=0; j < arr.length; j++){
      if(arr[j] > arr[j+1]){
        let temp = arr[j];
        arr[j] = arr[j+1];
        arr[j+1] = temp;
      }
    }
  }
  return arr;
}

function sortByItem(item){
  let sortedData;
  let minusone= -1;
  if(item['item'] === 'name'){
    sortedData = item['array'].sort(function(a,b){
      let x = a.name.toLowerCase();
      let y = b.name.toLowerCase();
      if(x>y){
        return 1;
      } 
      if(x<y){
        return minusone;
      }
      return 0;
    });
  }
  return sortedData;
}
