let timer = 5000;
function clickCounter(){
  let nickname = document.getElementById('nickname').value;
  let firstValue = localStorage.getItem('myClicks')? parseInt(localStorage.getItem('myClicks')) : 0;
  if(nickname === ''){
    alert('Empty nickname'); 
  } else {
    document.getElementById('bigbtn').addEventListener('click', function(){
      let currentValue = localStorage.getItem('myClicks') ? parseInt(localStorage.getItem('myClicks')) : 0;
      let newValue = currentValue + 1;
      localStorage.setItem('myClicks', newValue); 
    });
    setTimeout(function(){
      let result = localStorage.getItem('myClicks') - firstValue;
      alert('You clicked ' + result + ' times');
      window.sessionStorage.setItem(nickname, result);
      window.localStorage.setItem(nickname, result);
    }, timer);  
  }
}

function bestResult(){
  let clickArr = Object.values(sessionStorage);
  let newclickArr = [];
  for(let i=0; i<clickArr.length; i++){
    let clickCount = parseInt(clickArr[i]);
    newclickArr.push(clickCount);
  }
  let maxValue = Math.max(...newclickArr);
  if(maxValue > 0 ){
    alert('Best result is: ' + maxValue);
  } else{
    alert('Best result is: 0');
  } 
}

function bestResultForAllTime(){
  let clickObj = localStorage;
  let newclickobj={};
  delete clickObj['myClicks'];
  for(let i=0; i<clickObj.length; i++){
    let clickValue = parseInt(localStorage.getItem(localStorage.key(i)));
    let clickKey = localStorage.key(i);
    newclickobj[clickKey] = clickValue;
  }
  try{
    let maxKey = Object.keys(newclickobj).reduce((a, b) => newclickobj[a] > newclickobj[b] ? a : b);
    let maxValue = Object.entries(newclickobj).reduce((max, n) => n[1] > max[1] ? n : max);
    alert('Best result for the whole time is: ' + maxValue[1] + ' by ' + maxKey);
  } catch (err){
    alert('Best result for the whole time is: 0')
  }  
}

function clearBestResult(){
  sessionStorage.clear();
  alert('Best result is: 0')
}

function clearBestResultForAllTime(){
  localStorage.clear();
  alert('Best result for the whole time is: 0')
}

