function profit() {
  let money = prompt('Please enter initial amount of money: ');
  let years = prompt('Please enter number of years: ');
  let percent = prompt('Please enter percentage of a year: ');
  if (isNaN(money) || isNaN(years) || isNaN(percent) || money < 1000 || years < 1 || percent > 100) {
      alert('Invalid input data');
  } else{
    let total_amount = (money*Math.pow(1+percent/100, years)).toFixed(2);
    let total_profit = total_amount - money;
    alert('Initial amount: ' + money + '\nNumber of years: ' + years + 
    '\nPercentage of year: ' + percent + '\n' + '\nTotal profit: ' + total_profit + 
    '\nTotal amount: ' + total_amount);
  }
 }

