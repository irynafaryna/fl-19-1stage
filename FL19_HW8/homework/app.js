// #1
function extractCurrencyValue(param) {
  return param;
}

console.log(extractCurrencyValue('120 USD')); // 120
console.log(extractCurrencyValue('1283948234720742 EUR')); // 1283948234720742n


// #2

let object = {
    name: 'Ann',
    age: 16,
    hobbies: undefined,
    degree: null,
    isChild: false,
    isTeen: true,
    isAdult: false
}

function clearObject(obj) { 
    for (let key in obj) {
        if (!obj[key]) {
            delete obj[key];
        }
    }
    return obj;
}

console.log(clearObject(object)); // { name: 'Ann', age: 16, isTeen: true }


// #3

function getUnique(param) {
    let unique = Symbol(param);
    return unique;
} 

console.log(getUnique('Test')) // Symbol('Test')


// #4

function countBetweenTwoDays(startDate, endDate) {
    // let date1 = Date.UTC(startDate.getDate(), startDate.getMonth(), startDate.getFullYear());
    // let date2 = Date.UTC(endDate.getDate(), endDate.getMonth(), endDate.getFullYear());
    return Math.floor((endDate - startDate) / 1000*60*60*24);
//    let diffTime = Math.abs(endDate - startDate);
//    let diffDays = Math.ceil(diffTime/1000*60*60*2);
  // return " The difference between dates is: " + diffDays + " day(-s)"
}

console.log(countBetweenTwoDays('03/22/2022', '05/25/2022')); // The difference between dates is: 64 day(-s), 9 week(-s), 2 month(-s)


// #5

function filterArray(arr) {
    //with set:
    // let uniqueSet = [... new Set(arr)];
    // return uniqueSet;
    //without set:
    uniqueArray = arr.filter(function(item, pos) {
        return arr.indexOf(item) === pos;
    })
    return uniqueArray;
}

console.log(filterArray([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9])); // [1, 2, 3, 4, 5, 6, 7, 8, 9]
